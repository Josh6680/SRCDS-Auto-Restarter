How to use:
 - Place "SRCDS Auto Restarter.exe" and "settings.txt" in the same folder as "srcds.exe".
 - Edit the "settings.txt" file and put in your srcds command-line.
 - Run "SRCDS Auto Restarter.exe" and watch the automatic restarter do it's magic!

Notes:

The "settings.txt" file contains some configurable settings for the restarter.
Most of the settings are documented in the "settings.txt" file itself.
// Comments and blank lines are allowed, and will be ignored by the settings parser.

If the "settings.txt" file is missing,
the program will automatically create it and then close itself so that you can edit it.
