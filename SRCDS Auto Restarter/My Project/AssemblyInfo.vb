﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SRCDS Auto Restarter")> 
<Assembly: AssemblyDescription("Automatically restarts SRCDS (single-instance only).")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("SRCDS Auto Restarter")> 
<Assembly: AssemblyCopyright("Copyright © Josh 2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ec8e2d31-be91-4d3b-8992-5c536c92097d")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.4.0.0")>
<Assembly: AssemblyFileVersion("1.4.0.0")>
