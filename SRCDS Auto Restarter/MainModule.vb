﻿Imports System.Diagnostics.Process
Imports System.IO
Imports System.Threading
Imports System.Threading.Thread
Imports System.Windows.Forms
Imports Microsoft.VisualBasic.FileIO

Module MainModule
    Private ReadOnly newline As String = Environment.NewLine

    Private ReadOnly DefaultSettings As String =
        "// Auto Restarter Console Colors :)" + newline +
        "10" + newline +
        "0" + newline +
        newline +
        "// Auto Restarter Thread Priority" + newline +
        "BelowNormal" + newline +
        newline +
        "// Auto Restarter Thread Delay" + newline +
        "500" + newline +
        newline +
        "// SRCDS Process Priority" + newline +
        "AboveNormal" + newline +
        newline +
        "// SRCDS Command Line" + newline +
        "srcds.exe -console -insecure -norestart -timeout 0" + newline +
        newline +
        "// True to reload this file every time SRCDS restarts (useful for debugging command line)" + newline +
        "// If it was set to False then you need to restart the autorestarter when you set it to True" + newline +
        "False" + newline +
        newline +
        "// True to automatically determine the last loaded map and append (override)" + newline +
        "// the ""+map"" option on the command line when restarting SRCDS" + newline +
        "False" + newline +
        "// Set the path to the downloadlists folder used for determining the last loaded map" + newline +
        "hl2mp/downloadlists" + newline +
        "// Set the path to the maps folder used for checking the last loaded map exists" + newline +
        "hl2mp/maps" + newline +
        newline +
        "// App ID override for steam_appid.txt file, useful if you are running the server from a steam folder And your ingame status Is being set" + newline +
        "232370"

    Private AppPath As String = ""
    Private ErrorFile As String = "err.log"
    Private SettingsFile As String = "settings.txt"
    Private RestartFile As String = "!restart"
    Private Settings As List(Of String) = Nothing
    Private LoopThread As Thread = Nothing
    Private SRCDSHasRun As Boolean = False
    Public Running As Boolean = True
    Private exefile As String = ""
    Private exename As String = ""
    Private cmdline As String = ""

    Sub Main()
        ' Set console settings.
        Console.Title = "SRCDS Auto Restarter v" + My.Application.Info.Version.ToString(4) + " by Josh"
        Console.SetWindowPosition(0, 0)
        AddHandler Console.CancelKeyPress,
            Sub(sender As Object, e As Object)
                e.Cancel = True
                Running = False
            End Sub

        ' Get working directory.
        AppPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString
        ' Resolve path to file(s).
        SettingsFile = AppPath + SettingsFile
        ErrorFile = AppPath + ErrorFile

        ' If settings file doesn't exist, create it and quit.
        If My.Computer.FileSystem.FileExists(SettingsFile) = False Then
            My.Computer.FileSystem.WriteAllText(SettingsFile, DefaultSettings, True, Text.Encoding.ASCII)
            Environment.Exit(-1)
        End If

        ' Get settings from file.
        Settings = ReadSettings(SettingsFile)

        ' Set stuff from the settings.
        Console.ForegroundColor = CType([Enum].Parse(GetType(ConsoleColor), Settings(0), True), ConsoleColor)
        Console.BackgroundColor = CType([Enum].Parse(GetType(ConsoleColor), Settings(1), True), ConsoleColor)

        ' Lower this program's process priority - it's not important.
        Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal

        Msg("╔════════════════════════════════════════════════════════╗")
        Msg("║            SRCDS Auto Restarter Initialized.           ║")
        Msg("╠════════════════════════════════════════════════════════╣")
        Msg("║ * If you want to stop SRCDS, first close this program. ║")
        Msg("╚════════════════════════════════════════════════════════╝")

        cmdline = Settings(5).Substring(Settings(5).IndexOf(Char.Parse(" ")) + 1)
        exefile = Settings(5).Substring(0, Settings(5).IndexOf(Char.Parse(" ")))
        If Not exefile.Trim("""").EndsWith(".exe") Then
            MsgBox("Invalid command line detected! The program name must end with .exe" + vbNewLine + "SRCDS Auto Restarter cannot function and must now close.")
            Throw New InvalidOperationException("Invalid command line program: " + exefile.Trim("""") + " does not end with .exe")
            'Environment.Exit(1)
        End If
        exename = My.Computer.FileSystem.GetFileInfo(exefile.Trim("""")).Name.Replace(".exe", "")
        Msg("Using command line:")
        Msg(cmdline)

        Try
            ' Create the main thread.
            LoopThread = New Thread(AddressOf MainLoop)
            ' Set the priority from the settings.
            LoopThread.Priority = CType([Enum].Parse(GetType(ThreadPriority), Settings(2), True), ThreadPriority)

            ' Start the thread.
            LoopThread.Start()

            ' Wait for the thread to terminate.
            LoopThread.Join()
        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText(ErrorFile, ex.ToString + newline, True, Text.Encoding.UTF8)
            Msg("ERROR: " + ex.ToString())
            Environment.Exit(-2)
        Finally
            Environment.Exit(0)
        End Try
    End Sub

    Sub MainLoop()
        Msg("Checking for Engine Errors...")
        Dim p As Process() = Nothing
        Dim found As Boolean = False
        Try
            Dim delay As Integer = CInt(Settings(3))
            Msg("Restart thread delay: " + delay.ToString)
            Do
                found = False
                p = GetProcesses()
                For i As Integer = 0 To p.Count - 1
                    If p(i).ProcessName = exename Then
                        found = True
                        If SRCDSHasRun = False Then
                            Msg("SRCDS is already running.", "╠═-")
                            SRCDSHasRun = True
                        End If
                        Dim windowTitle = p(i).MainWindowTitle()
                        If windowTitle.StartsWith("Engine Error", False, Globalization.CultureInfo.CurrentCulture) Then
                            p(i).Kill()
                            StartSRCDS(True)
                            If windowTitle <> "Engine Error" Then
                                Msg("WARNING: SRCDS Engine Error """ + windowTitle + """ detected, restarted.", "╠═-")
                            Else
                                Msg("WARNING: SRCDS Engine Error detected, restarted.", "╠═-")
                            End If
                        ElseIf My.Computer.FileSystem.FileExists(AppPath + RestartFile) Then
                            p(i).Kill()
                            StartSRCDS(True)
                            Msg("WARNING: Emergency restart file detected, restarted.", "╠═-")
                            'My.Computer.FileSystem.RenameFile(AppPath + RestartFile, "!restarted")
                            My.Computer.FileSystem.DeleteFile(AppPath + RestartFile, UIOption.OnlyErrorDialogs, RecycleOption.DeletePermanently, UICancelOption.DoNothing)
                        End If
                    End If
                Next
                If found = False Then
                    StartSRCDS()
                End If
                Sleep(delay)
            Loop While Running
            Msg("Closing...")
        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText(ErrorFile, ex.ToString + newline, True, Text.Encoding.UTF8)
            Msg("ERROR: " + ex.ToString)
        End Try
    End Sub

    Sub StartSRCDS(Optional ByVal suppressmsg As Boolean = False)
        exefile = Settings(5).Substring(0, Settings(5).IndexOf(Char.Parse(" ")))
        If Not exefile.Trim("""").EndsWith(".exe") Then
            MsgBox("Invalid command line detected! The program name must end with .exe" + vbNewLine + "SRCDS Auto Restarter cannot function and must now close.")
            Throw New InvalidOperationException("Invalid command line program: " + exefile.Trim("""") + " does not end with .exe")
            'Environment.Exit(1)
        End If
        exename = My.Computer.FileSystem.GetFileInfo(exefile.Trim("""")).Name.Replace(".exe", "")
        cmdline = Settings(5).Substring(Settings(5).IndexOf(Char.Parse(" ")) + 1)
        'MsgBox("'" + exename + "'")
        'MsgBox("'" + cmdline + "'")
        If SRCDSHasRun And Boolean.Parse(Settings(6)) Then
            Settings = ReadSettings(SettingsFile)

            ' Set stuff from the settings.
            Console.ForegroundColor = CType([Enum].Parse(GetType(ConsoleColor), Settings(0), True), ConsoleColor)
            Console.BackgroundColor = CType([Enum].Parse(GetType(ConsoleColor), Settings(1), True), ConsoleColor)

            LoopThread.Priority = CType([Enum].Parse(GetType(ThreadPriority), Settings(2), True), ThreadPriority)

            exefile = Settings(5).Substring(0, Settings(5).IndexOf(Char.Parse(" ")))
            If Not exefile.Trim("""").EndsWith(".exe") Then
                MsgBox("Invalid command line detected! The program name must end with .exe" + vbNewLine + "SRCDS Auto Restarter cannot function and must now close.")
                Throw New InvalidOperationException("Invalid command line program: " + exefile.Trim("""") + " does not end with .exe")
                'Environment.Exit(1)
            End If
            exename = My.Computer.FileSystem.GetFileInfo(exefile.Trim("""")).Name.Replace(".exe", "")
            cmdline = Settings(5).Substring(Settings(5).IndexOf(Char.Parse(" ")) + 1)
            Msg("Using command line:")
            Msg(cmdline)

            Msg("Auto Restarter settings.txt reloaded.", "╠═-")
        End If

        File.WriteAllText("steam_appid.txt", Settings(10))

        If CBool(Settings(7)) Then
            Dim directory = New DirectoryInfo(Settings(8))
            Dim latestFile = (From f In directory.GetFiles("*.lst", IO.SearchOption.TopDirectoryOnly) Order By f.LastWriteTime Descending Select f).First()
            Dim mapName As String = latestFile.Name.Remove(latestFile.Name.Length - 4, 4)
            If File.Exists(Path.Combine(Settings(9), mapName + ".bsp")) Then
                cmdline += " +map " + mapName
                Msg("Overriding +map on command line to last loaded map: " + mapName, "╠═-")
            Else
                Msg("WARNING: Last loaded map file """ + mapName + ".bsp"" does not exist! Using default", "╠═-")
            End If
        End If
        Process.Start(exefile, cmdline).PriorityClass = CType([Enum].Parse(GetType(ProcessPriorityClass), Settings(4), True), ProcessPriorityClass)
        If suppressmsg = False Then
            If SRCDSHasRun Then
                Msg("WARNING: SRCDS crashed or closed, restarted.", "╠═-")
            Else
                Msg("SRCDS started.", "╠═-")
                If SRCDSHasRun = False Then
                    SRCDSHasRun = True
                End If
            End If
        End If
    End Sub

    Function Msg(ByVal message As String, Optional ByVal seperator As String = "║ ") As Boolean
        Console.WriteLine("({0}) " + seperator + message, GetTimeString())
    End Function

    Function GetTimeString() As String
        Return Format(Now, "hh:mm:ss.fff")
    End Function

    Function ReadSettings(ByVal file As String) As List(Of String)
        Dim contents = My.Computer.FileSystem.ReadAllText(file, Text.Encoding.ASCII)
        Dim lines() As String = (New RichTextBox With {.Text = contents}).Lines
        Dim ret As List(Of String) = New List(Of String)
        For i As Integer = 0 To lines.Length - 1
            If Not (lines(i).StartsWith("//")) And Not (lines(i).Equals("")) Then
                ret.Add(lines(i))
            End If
        Next
        Return ret
    End Function

End Module