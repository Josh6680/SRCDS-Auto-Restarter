﻿// Auto Restarter Console Colors :)
10
0

// Auto Restarter Thread Priority
BelowNormal
// Lowest = 0 (not tested)
// BelowNormal = 1
// Normal = 2
// AboveNormal = 3
// Highest = 4 (not recommended)

// Auto Restarter Thread Delay
500

// SRCDS Process Priority
AboveNormal
// Idle = 64 (not recommended)
// BelowNormal = 16384
// Normal = 32
// AboveNormal = 32768
// High = 128
// RealTime = 256 (not recommended)

// SRCDS Command Line
"C:/valveserver/server/srcds.exe" -console -insecure -game hl2mp -norestart -timeout 0 +map gm_flatgrass +maxplayers 8

// True to reload this file every time SRCDS restarts (useful for debugging command line)
// If it was set to False then you need to restart the autorestarter when you set it to True
False

// True to automatically determine the last loaded map and append (override)
// the "+map" option on the command line when restarting SRCDS
True
// Set the path to the downloadlists folder used for determining the last loaded map
C:/valveserver/server/hl2mp/downloadlists
// Set the path to the maps folder used for checking the last loaded map exists
C:/valveserver/server/hl2mp/maps

// App ID override for steam_appid.txt file, useful if you are running the server from a steam folder and your ingame status is being set
232370